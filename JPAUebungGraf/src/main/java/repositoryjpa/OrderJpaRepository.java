package repositoryjpa;

import java.util.List;

import org.springframework.stereotype.Repository;

import domain.Order;

@Repository
public class OrderJpaRepository extends AbstractJpaRepository<Order>{

	@Override
	public List<Order> findAll() {
		return entityManager().createQuery("SELECT order FROM Orders order", Order.class).getResultList();
	}

	@Override
	public Order findById(Long id) {
		return entityManager().find(Order.class, id);
	}

}
