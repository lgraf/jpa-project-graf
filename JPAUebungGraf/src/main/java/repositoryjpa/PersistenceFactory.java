package repositoryjpa;

public interface PersistenceFactory {
	DelivererJpaRepository delivererJpaRepository();
	DeliveryJpaRepository deliveryJpaRepository();
	OrderJpaRepository orderJpaRepository();
	PersonJpaRepository personJpaRepository();
	ProductJpaRepository productJpaRepository();
	ProductquantityJpaRepository productquantityJpaRepository();
	UserJpaRepository userJpaRepository();
}
