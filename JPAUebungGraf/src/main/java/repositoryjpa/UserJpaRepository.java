package repositoryjpa;

import java.util.List;

import org.springframework.stereotype.Repository;

import domain.User;

@Repository
public class UserJpaRepository extends AbstractJpaRepository<User>{

	@Override
	public List<User> findAll() {
		return entityManager().createQuery("SELECT user FROM users user", User.class).getResultList();
	}

	@Override
	public User findById(Long id) {
		return entityManager().find(User.class, id);
	}

}
