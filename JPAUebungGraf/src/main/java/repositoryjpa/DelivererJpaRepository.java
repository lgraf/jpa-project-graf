package repositoryjpa;

import java.util.List;

import org.springframework.stereotype.Repository;

import domain.Deliverer;

@Repository
public class DelivererJpaRepository extends AbstractJpaRepository<Deliverer>{

	@Override
	public List<Deliverer> findAll() {
		return entityManager().createQuery("SELECT deliverer FROM Deliverers deliverer", Deliverer.class).getResultList();
	}

	@Override
	public Deliverer findById(Long id) {
		return entityManager().find(Deliverer.class, id);
	}

}
