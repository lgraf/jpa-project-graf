package repositoryjpa;

import java.util.List;

import org.springframework.stereotype.Repository;

import domain.Person;

@Repository
public class PersonJpaRepository extends AbstractJpaRepository<Person>{

	@Override
	public List<Person> findAll() {
		return entityManager().createQuery("SELECT person FROM persons person", Person.class).getResultList();
	}

	@Override
	public Person findById(Long id) {
		return entityManager().find(Person.class, id);
	}

}
