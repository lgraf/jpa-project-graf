package repositoryjpa;

import java.util.List;

import org.springframework.stereotype.Repository;

import domain.Delivery;

@Repository
public class DeliveryJpaRepository extends AbstractJpaRepository<Delivery>{

	@Override
	public List<Delivery> findAll() {
		return entityManager().createQuery("SELECT delivery FROM Deliveries delivery", Delivery.class).getResultList();
	}

	@Override
	public Delivery findById(Long id) {
		return entityManager().find(Delivery.class, id);
	}

}
