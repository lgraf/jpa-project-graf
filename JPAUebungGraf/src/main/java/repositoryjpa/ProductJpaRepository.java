package repositoryjpa;

import java.util.List;

import org.springframework.stereotype.Repository;

import domain.Product;

@Repository
public class ProductJpaRepository extends AbstractJpaRepository<Product>{

	public List<Product> findAll() {
		return entityManager().createQuery("SELECT product FROM Products product", Product.class).getResultList();
	}

	public Product findById(Long id) {
		return entityManager().find(Product.class, id);
	}

}
