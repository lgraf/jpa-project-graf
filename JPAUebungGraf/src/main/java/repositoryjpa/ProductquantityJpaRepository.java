package repositoryjpa;

import java.util.List;

import org.springframework.stereotype.Repository;

import domain.Productquantity;

@Repository
public class ProductquantityJpaRepository extends AbstractJpaRepository<Productquantity>{

	@Override
	public List<Productquantity> findAll() {
		return entityManager().createQuery("SELECT productquantity FROM productquantities productquantity", Productquantity.class).getResultList();
	}

	@Override
	public Productquantity findById(Long id) {
		return entityManager().find(Productquantity.class, id);
	}

}
