package webapp.product;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;

import domain.Product;
import webapp.ContentPanel;

public class ProductPanel extends ContentPanel{
	private List<Product> productList = new ArrayList<>();

    public ProductPanel(String id) {
        super(id);
        add(new ProductForm("productForm", productList));

        add(new PropertyListView<Product>("productList", productList) {
            @Override
            public void populateItem(final ListItem<Product> listItem) {
                listItem.add(new Label("name"));
                listItem.add(new MultiLineLabel("description"));
                //listItem.add(new MultiLineLabel("birthDate"));
            }
        }).setVersioned(false);
    }
}
