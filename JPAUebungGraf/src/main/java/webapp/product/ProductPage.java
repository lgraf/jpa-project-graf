package webapp.product;

import org.apache.wicket.Component;

import webapp.DeliveryservicePage;

public class ProductPage extends DeliveryservicePage{
	protected Component contentPanel() {
        return new ProductPanel("productPanel");
    }
}
