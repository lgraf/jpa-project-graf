package webapp.product;

import java.util.Date;
import java.util.List;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import repository.ProductRepository;
import domain.Product;

public class ProductForm extends Form<Product>{
	protected final Logger logger = LoggerFactory.getLogger(getClass());
    private List<Product> productList;

    @SpringBean
    private ProductRepository productRepository;

    public ProductForm(String id, List<Product> productList) {
        super(id, new CompoundPropertyModel<Product>(new Product("Swag", "Yolo", 5)));
        add(new TextField<>("name"));
        add(new TextField<>("description"));
        //add(new TextField<>("birthDate"));
        this.productList = productList;
    }

    @Override
    public final void onSubmit() {
        Product product = getModelObject();
        productRepository.save(product);
        productList.add(0, product);
    }
}
