package webapp;

import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;

import webapp.product.ProductPage;

public class NavigationPanel extends Panel{
	public NavigationPanel(String id) {
        super(id);
        add(new Link("navigateHelloWorld") {
            @Override
            public void onClick() {
                setResponsePage(HelloWorldPage.class);
            }
        });
        add(new Link("navigateProduct") {
            @Override
            public void onClick() {
                setResponsePage(ProductPage.class);
            }
        });
    }
}
