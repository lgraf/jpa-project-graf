package webapp;

import javax.servlet.annotation.WebFilter;

import org.apache.wicket.protocol.http.WicketFilter;
import javax.servlet.annotation.WebInitParam;

@WebFilter(value = "/web/*", initParams = {
        @WebInitParam(name = "applicationClassName", value = "webapp.DeliveryserviceApplication"),
        @WebInitParam(name = "filterMappingUrlPattern", value = "/web/*")})
public class DeliveryserviceFilter extends WicketFilter{

}
