package repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import domain.Order;
import domain.User;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long>{
	List<Order> findByUser(User user);
}
