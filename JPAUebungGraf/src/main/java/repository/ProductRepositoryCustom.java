package repository;

import java.util.List;

import domain.Product;

public interface ProductRepositoryCustom {
	List<Product> findWithQueryDSL(String name);
}
