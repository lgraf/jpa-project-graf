package repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import domain.Deliverer;

@Repository
public interface DelivererRepository extends CrudRepository<Deliverer, Long>{

}
