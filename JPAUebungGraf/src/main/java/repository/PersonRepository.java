package repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import domain.Person;

@Repository
public interface PersonRepository extends CrudRepository<Person, Long>{
	List<Person> findByName(String name);
	List<Person> findByEmail(String email);
}
