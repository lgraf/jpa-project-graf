package repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import domain.Deliverer;
import domain.Delivery;

@Repository
public interface DeliveryRepository extends CrudRepository<Delivery, Long>{
	List<Delivery> findByDeliverydate(Date deliverydate);
	List<Delivery> findByDeliverer(Deliverer deliverer);
}
