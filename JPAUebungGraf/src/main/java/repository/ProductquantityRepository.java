package repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import domain.Productquantity;

@Repository
public interface ProductquantityRepository extends CrudRepository<Productquantity, Long>{

}
