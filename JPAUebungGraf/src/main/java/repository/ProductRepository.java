package repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import domain.Product;
import domain.ProductCategory;

@Repository
public interface ProductRepository extends ProductRepositoryCustom, CrudRepository<Product, Long>{
	List<Product> findByName(String name);
	List<Product> findByCategory(ProductCategory category);
}
