package repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.mysema.query.jpa.impl.JPAQuery;

import domain.Product;
import domain.QProduct;
import repository.ProductRepositoryCustom;

public class ProductRepositoryImpl implements ProductRepositoryCustom{

	@PersistenceContext
	EntityManager entityManager;
	
	public List<Product> findWithQueryDSL(String name) {
		JPAQuery query=new JPAQuery(entityManager);
		QProduct product=QProduct.product;
		query.from(product).where(product.name.eq(name));
		return query.list(product);
	}

}
