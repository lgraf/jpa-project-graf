package domain;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import ensure.Ensure;

@Entity
@Table(name="products")
public class Product extends BasePersistable{
	private static final long serialVersionUID = 325745886538486162L;
	
	private String name;
	private String description;
	private double price;
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="product")
	private Collection<Productquantity> quantity;
	
	@Enumerated(EnumType.STRING)
	private ProductCategory category;
	
	protected Product(){
		
	}
	public Product(String name, String description, double price){
		Ensure.notEmpty("name", name);
		this.name=name;
		this.description=description;
		this.price=price;
		this.quantity=new ArrayList<Productquantity>();
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public ProductCategory getCategory() {
		return category;
	}
	public void setCategory(ProductCategory category) {
		this.category = category;
	}
	
}
