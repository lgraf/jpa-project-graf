package domain;

public enum ProductCategory {
	food, drinks, books, music, clothes;
}
