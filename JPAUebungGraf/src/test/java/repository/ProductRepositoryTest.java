package repository;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import domain.Product;

@ContextConfiguration(classes = RepositoryTestConfiguration.class)
public class ProductRepositoryTest extends AbstractJUnit4SpringContextTests{
	@Autowired
    ProductRepository productRepository;

    @Before
    public void setup() {
        // remove existing data
        productRepository.deleteAll();

        // create test data
        productRepository.save(new Product("Pizza Salami", "Mit Salami", 5));
        productRepository.save(new Product("Pizza Cardinale", "Mit Schinken", 10));
        productRepository.save(new Product("Swag", "sehr Yolo", 15));
    }

    @Test
    public void testFindByName() {
        // given

        // when
        List<Product> byName = productRepository.findByName("Pizza Salami");

        // then
        Assert.assertNotNull(byName);
        Assert.assertEquals(1, byName.size());
        Assert.assertNotNull(byName.get(0));
    }

    @Test
    public void testQueryDslQuery() {
        // given

        // when
        List<Product> byName = productRepository.findWithQueryDSL("Swag");

        // then
        Assert.assertNotNull(byName);
        Assert.assertEquals(1, byName.size());
        Assert.assertNotNull(byName.get(0));
    }
}
