package domain;

import org.junit.Test;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

public class ProductTest extends AbstractDomainPersistenceTest{
	@Test
    public void persistProduct() {
        // given
        Product product = new Product("Pizza", "Yes", 5);
        assertThat(product.getId(), is(nullValue()));
        // when
        entityManager().persist(product);
        // then
        assertThat(product.getId(), is(notNullValue()));
    }
}
