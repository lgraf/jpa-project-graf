package domain;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(value = Parameterized.class)
public class ProductConstructorTest {
	private String name;

    private String description;

    private double price;

    public ProductConstructorTest(String name, String description, double price) {
        this.name = name;
        this.description = description;
        this.price = price;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{
                {null, null, 0}, 
                {null, "Hugo", 0}, 
                {null, null, 0}, 
                {null, "description", 0}};
        return Arrays.asList(data);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenCreatingWithNullArguments() {
        new Product(name, description, price);
    }
}
