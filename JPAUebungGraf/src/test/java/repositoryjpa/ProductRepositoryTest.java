package repositoryjpa;

import org.junit.Assert;
import org.junit.Test;

import domain.Product;

public class ProductRepositoryTest extends AbstractJpaRepositoryTest{
	@Test
    public void verifyFindByUnknownId() {
        ProductJpaRepository productJpaRepository = new ProductJpaRepository();
        productJpaRepository.setEntityManager(entityManager);

        Product product = productJpaRepository.findById(1l);

        Assert.assertNull(product);
    }

    @Test
    public void verifyFindById() {
        ProductJpaRepository productJpaRepository = new ProductJpaRepository();
        productJpaRepository.setEntityManager(entityManager);

        Product product = new Product("Swag", "Yolo", 10);
        productJpaRepository.persist(product);

        Product product2 = productJpaRepository.findById(product.getId());

        Assert.assertNotNull(product2);
        Assert.assertEquals(product, product2);
    }
}
