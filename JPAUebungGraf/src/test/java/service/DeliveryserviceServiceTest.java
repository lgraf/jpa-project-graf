package service;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

@ContextConfiguration(classes = ServiceTestConfiguration.class)
public class DeliveryserviceServiceTest extends AbstractJUnit4SpringContextTests{
	@Autowired
    DeliveryserviceService deliveryserviceService;

    @Test
    public void aTest() {
        Assert.assertNotNull(deliveryserviceService);
    }
}
